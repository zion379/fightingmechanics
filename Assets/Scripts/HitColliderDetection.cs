﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HitColliderDetection : MonoBehaviour {

	public Collider[] attackHitBoxes;
	public Collider[] blockHitBoxes;

	public bool Head_Punch_Connect_R;
	public bool Waist_Kick_Connect;

	private string HitColliderName;
	private Animator anim;
	private Animator globalAnim;

	void Start() {
		anim = GetComponent<Animator> ();
	}

	private void LaunchAttack (Collider col) {
		Debug.Log ("LaunchAttack Working");
		Collider[] cols = Physics.OverlapBox(col.bounds.center , col.bounds.extents, col.transform.rotation ,LayerMask.GetMask("Hitboxes"));
		foreach (Collider c in cols) {


			if (c.transform.root != this.transform) {
				
				HitColliderName = c.name;

				Animator[] anim = c.GetComponentsInParent<Animator> ();

				// Add all the trigger hit animation states here.
				switch(HitColliderName){
				case "Head":
					Debug.Log ("Head hit");
					Head_Punch_Connect_R = true;

					foreach (Animator animator in anim) {
						animator.SetBool ("Head_Punch_Connect_R", Head_Punch_Connect_R);
						globalAnim = animator;
					}
					//Debug.Log (Head_Punch_Connect_R);
					break;
				
				case "Spine":
					Debug.Log ("Spine Hit");
					Waist_Kick_Connect = true;
					foreach (Animator animator in anim) {
						animator.SetBool ("Waist_Kick_Connect", Waist_Kick_Connect);
						globalAnim = animator;
					}
					Debug.Log (Waist_Kick_Connect);
						
					break;

				case "Hip":
					Debug.Log ("Hip Hit");
					Waist_Kick_Connect = true;
					foreach (Animator animator in anim) {
						animator.SetBool ("Waist_Kick_Connect", Waist_Kick_Connect);
						globalAnim = animator;
					}
					Debug.Log (Waist_Kick_Connect);
					break;

				default:
					Debug.Log ("nothing so far");
					Debug.Log (c.name);
					break;
				}
			}

			if (c.transform.root == this.transform)
				continue;
		}
	}

	private void BlockAttack(Collider col) {
		Collider[] cols = Physics.OverlapBox(col.bounds.center, col.bounds.extents, col.transform.rotation, LayerMask.GetMask("Blockboxes"));
		foreach (Collider c in cols) {

			// make sure to add Working Blocking feature here.
			if (c.transform.root == transform)
				continue;
		}
	}

	// not sure if im using this function?
	private void CollidersHit() {
		switch(HitColliderName){
		case "Head":
			Debug.Log ("Head hit");
			Head_Punch_Connect_R = true;
			anim.SetBool ("Head_Punch_Connect_R", Head_Punch_Connect_R);
			break;
		default:
			Debug.Log ("nothing so far");
			break;
		}
	}

	void ClearHitColliderName() {
		HitColliderName = "";
		Head_Punch_Connect_R = false;
		Debug.Log (Head_Punch_Connect_R);
	}

	void Update() {

		if (Input.GetKeyDown (KeyCode.P)) {
			LaunchAttack (attackHitBoxes[0]);

		}

		if (Input.GetKeyDown (KeyCode.K)) {
			LaunchAttack (attackHitBoxes [1]);
		}
		AnimationsCurrentlyPlaying ();

	}

	void AnimationsCurrentlyPlaying() {
		if (globalAnim.GetCurrentAnimatorStateInfo (0).IsName ("Base Layer.KB_Hit_m_HighRight_Med")) { // that current animation is running then set Head punch bool false to stop animation
			Head_Punch_Connect_R = false;
			globalAnim.SetBool ("Head_Punch_Connect_R", Head_Punch_Connect_R);
		} else {
			//Debug.Log ("hit animation is not playing");
		}
			

		// the rest of the ending functions for hit animations here.
		if (globalAnim.GetCurrentAnimatorStateInfo (0).IsName ("Base Layer.KB_Hit_m_LowRight_Med")) {
			Waist_Kick_Connect = false;
			globalAnim.SetBool ("Waist_Kick_Connect", Waist_Kick_Connect);
			Debug.Log ("Stoping waist hit animation");
		} else {
			//Debug.Log("Waist kick animation not playing");
		}
	}


}
