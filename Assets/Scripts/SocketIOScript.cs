using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Newtonsoft.Json;
using System;

public class ChatData
{
	public string id;
	public string possition;
	public string type;
};

public class SocketIOScript : MonoBehaviour
{
	//NOTE: adding health later - reason for commenting
	public GameObject player;
	//public GameObject playerTwo;
	//public GameObject bullet;
	public Text txt;
	public int NetworkSpeed = 5;
	public string ip = "localhost";
	public string port = "8000";

	private ChatData pos;
	private int x = 0;
	private int currentTime;
	// Use this for initialization
	IEnumerator Start ()
	{
		WebSocket w = new WebSocket (new Uri ("ws://" + ip + ":" + port));
		yield return StartCoroutine (w.Connect ());
		int i = 0;
		string myId = UnityEngine.Random.Range (0.0f, 1000000.0f).ToString ();
		w.SendString ("START_" + myId);
		GameObject myPlayer;
		Debug.Log ("DEFULT");
		myPlayer = Instantiate (player, new Vector3 (0, 10, 0), Quaternion.identity);
		myPlayer.name = myId;
		//myPlayer.gameObject.GetComponent<Renderer> ().material.color = Color.blue;
			
		while (true) {
			if ((int)((Time.time % 60) * NetworkSpeed) >= x) {
				//Debug.Log (Time.time % 60);
				w.SendString (GameObject.Find (myId).transform.position.ToString () + "_" + myId);
				x++;
			}

			string reply = w.RecvString ();
			if (reply != null) {
				//Debug.Log (reply);
				string str = reply.ToString ();
				pos = JsonConvert.DeserializeObject<ChatData> (str);

				switch (pos.type) {
				case "possition":
					//displaying it
					txt.text = pos.possition + " lag: " + (x - (int)((Time.time % 60) * NetworkSpeed)).ToString ();

					Debug.Log ("Possition Received: "); // + pos.possition);
					if (pos.id == myId) {
						Debug.Log ("our own player, doing nothing");
						//w.SendString ("health_" + GameObject.Find (myId).GetComponent <Health> ().currentHealth.ToString () + "_" + myId);
					} else {
						if (GameObject.Find (pos.id)) {
							Debug.Log ("player exists moving them "); // + pos.id);
							GameObject.Find (pos.id).transform.position = StringToVector2 (pos.possition);
							//w.SendString ("health_" + GameObject.Find (pos.id).GetComponent <Health> ().currentHealth.ToString () + "_" + pos.id);
							//Debug.Log ("Sent Health " + GameObject.Find (pos.id).GetComponent <Health> ().currentHealth);
						} else {
							Debug.Log ("player does not exist, creating them with name " + pos.id);
							if (pos.possition == "START") {
								Debug.Log ("START");
							} else {
								var otherplayer = Instantiate (player, StringToVector2 (pos.possition), Quaternion.identity);
								otherplayer.name = pos.id;
								otherplayer.GetComponentInChildren <Camera> ().enabled = false;
								otherplayer.GetComponent <Rigidbody> ().isKinematic = true; 
								otherplayer.GetComponent <CharacterController> ().enabled = true; 
							}
						}
					}
					break;

				case "health":
					Debug.Log ("Health Received! --------------------- "); // + pos.possition);
					int healthInt;
					int.TryParse (pos.possition, out healthInt);

					Debug.Log ("health to take: " + healthInt);

					//var health = GameObject.Find (pos.id).GetComponent<Health> ();

					//Debug.Log ("HEALTH " + health.currentHealth);

//					if (health != null) {
//						//health.SetHealth (healthInt);
//					}
					break;
				}
			}
			if (w.error != null) {
				Debug.LogError ("Error: " + w.error);
				break;
			}
			yield return 0;
		}
		w.Close ();
	}

	public static Vector2 StringToVector2 (string sVector)
	{
		// Remove the parentheses
		if (sVector.StartsWith ("(") && sVector.EndsWith (")")) {
			sVector = sVector.Substring (1, sVector.Length - 2);
		}

		// split the items
		string[] sArray = sVector.Split (',');

		// store as a Vector3
		Vector3 result = new Vector3 (
			                 float.Parse (sArray [0]),
			                 float.Parse (sArray [1]),
			                 float.Parse (sArray [2]));

		return result;
	}
}
