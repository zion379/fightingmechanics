﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour {

	public float speed = 2;
	public Vector3 jumpHeight;

    private Rigidbody rbody;
    private float Hinput;
    private float Vinput;
    private Vector2 VelocityVector;
	public Animator anim;
	private HitColliderDetection hitColliderDetection;

	private bool jumping;
	private bool Block_Start;
	private bool Block_Loop;
	private bool Block_End;
	private bool punch;
	private bool kick;
	private bool Speacial01;



    void Awake() {
        rbody = GetComponent<Rigidbody>();
		anim = GetComponent<Animator> ();
    }

    void Update() {
        Movement();
		Blocking();
		Punching ();
		Kicking ();
		Speacials ();
		//HandleHits ();
    }

	void OnCollisionEnter(Collision collision) {
		//Debug.Log ("Standing");
	}

	void OnCollisionExit(Collision collision) {
		//Debug.Log ("Jumping");
	}

    void Movement() {
        Hinput = Input.GetAxis("Horizontal");
        //Vinput = Input.GetAxis("Vertical");

		anim.SetFloat("Hinput", Hinput);

		rbody.transform.Translate( 0f, 0f , (Hinput * Time.deltaTime * speed));

		if (Input.GetKeyDown (KeyCode.UpArrow) || Input.GetKeyDown (KeyCode.W)) {
			
			//rbody.AddForce(Vector2.up * speed);
			//rbody.transform.position += new Vector3(0,10,0);
			rbody.transform.Translate(0,jumpHeight.y,0);
			jumping = true;
			anim.SetBool ("Jumping", jumping);
			//rbody.AddForce (jumpHeight, ForceMode.Impulse);
			Debug.Log ("Adding force");
		} else {
			jumping = false;
			anim.SetBool ("Jumping", jumping);
		}
		//rbody.transform.Translate = VelocityVector;
    }

	void Blocking() {
		if (Input.GetKeyDown (KeyCode.B)) {
			//Debug.Log ("Blocking");
			Block_Start = true;
			anim.SetBool ("Block_Start", Block_Start);
		} else if(Input.GetKeyUp (KeyCode.B)) {
			Block_Start = false;
			anim.SetBool ("Block_Start", Block_Start);
		}
	}

	void Punching() {
		if (Input.GetKeyDown (KeyCode.P)) {
			//Debug.Log ("Punching");
			punch = true;
			anim.SetBool ("Punch", punch);
		} else if (Input.GetKeyUp (KeyCode.P)) {
			punch = false;
			anim.SetBool ("Punch", punch);
		}
	}

	void Kicking() {
		if (Input.GetKeyDown (KeyCode.K)) {
			//Debug.Log ("Kickng");
			kick = true;
			anim.SetBool ("Kick", kick);
		} else if (Input.GetKeyUp (KeyCode.K)) {
			kick = false;
			anim.SetBool ("Kick", kick);
		}
	}

	void Speacials() {
		if (Input.GetKeyDown (KeyCode.S)) {
			Speacial01 = true;
			anim.SetBool ("Speacial01", Speacial01);
			//Debug.Log ("Speacial Attack");
		} else if (Input.GetKeyUp (KeyCode.S)) {
			Speacial01 = false;
			anim.SetBool ("Speacial01", Speacial01);
		}
	}
		
		
}
